package com.minkhantlu.speedtestbenchmark;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by minkhantlu on 10/26/18.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
