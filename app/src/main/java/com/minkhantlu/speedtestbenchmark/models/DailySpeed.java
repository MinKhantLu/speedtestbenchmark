package com.minkhantlu.speedtestbenchmark.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by minkhantlu on 10/26/18.
 */

public class DailySpeed extends RealmObject{
    @PrimaryKey
    public String id;

    public String date;
    public  RealmList<Speed> speeds;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
