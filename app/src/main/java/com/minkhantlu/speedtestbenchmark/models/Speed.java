package com.minkhantlu.speedtestbenchmark.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by minkhantlu on 10/26/18.
 */

public class Speed extends RealmObject{

    @PrimaryKey
    private String id;

    private float downloadSpeed;
    private float uploadSpeed;
    private float ping;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(float downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public float getUploadSpeed() {
        return uploadSpeed;
    }

    public void setUploadSpeed(float uploadSpeed) {
        this.uploadSpeed = uploadSpeed;
    }

    public float getPing() {
        return ping;
    }

    public void setPing(float ping) {
        this.ping = ping;
    }
}
