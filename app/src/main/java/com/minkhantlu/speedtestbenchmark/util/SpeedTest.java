package com.minkhantlu.speedtestbenchmark.util;

import java.util.Random;

/**
 * Created by minkhantlu on 10/26/18.
 */

public class SpeedTest {


    public Float downloadSpeed() {
        Random random = new Random();
        float value = 200 + random.nextFloat() * (400 - 200);
        return value;
    }

    public Float uploadSpeed() {
        Random random = new Random();
        float value = 200 + random.nextFloat() * (400 - 200);
        return value;
    }

    public Float ping() {
        Random random = new Random();
        float value = 1 + random.nextFloat() * (10 - 1);
        return value;
    }

}
