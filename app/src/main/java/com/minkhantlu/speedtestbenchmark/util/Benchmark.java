package com.minkhantlu.speedtestbenchmark.util;

import com.minkhantlu.speedtestbenchmark.models.DailySpeed;
import com.minkhantlu.speedtestbenchmark.models.Speed;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by minkhantlu on 10/26/18.
 */

public class Benchmark {
    public Float downloadSpeed = 0f;
    public Float uploadSpeed = 0f;
    public Float ping = 0f;

    public Benchmark() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                hourly();
            }
        };
        timer.schedule(task, 0l, 1 * 3600 * 1000);
    }

    public void hourly() {

        SpeedTest speedTest = new SpeedTest();
        downloadSpeed = speedTest.downloadSpeed();
        uploadSpeed = speedTest.uploadSpeed();
        ping = speedTest.ping();

        //store into realmStorage
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        String strDate = DateFormat.getDateInstance().format(new Date());
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed == null) {
            dailySpeed = realm.createObject(DailySpeed.class, UUID.randomUUID().toString());
            dailySpeed.setDate(strDate);
        }

        Speed speed = new Speed();
        speed.setId(UUID.randomUUID().toString());
        speed.setDownloadSpeed(downloadSpeed);
        speed.setUploadSpeed(uploadSpeed);
        speed.setPing(ping);

        dailySpeed.speeds.add(speed);
        realm.copyToRealmOrUpdate(dailySpeed);
        realm.commitTransaction();
        realm.close();
        //end
    }

    public float download_average(Date date) {

        Realm realm = Realm.getDefaultInstance();
        Float total = 0F;
        ArrayList<Speed> speeds = new ArrayList<>();
        String strDate = DateFormat.getDateInstance().format(date);
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed != null) {
            speeds.addAll(dailySpeed.speeds);
            for (int i = 0; i < speeds.size(); i++) {
                total += speeds.get(i).getDownloadSpeed();
            }
            realm.close();
            return total / speeds.size();
        }
        return 0f;

    }

    public float upload_average(Date date) {
        Realm realm = Realm.getDefaultInstance();
        Float total = 0F;
        ArrayList<Speed> speeds = new ArrayList<>();
        String strDate = DateFormat.getDateInstance().format(date);
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed != null) {
            speeds.addAll(dailySpeed.speeds);
            for (int i = 0; i < speeds.size(); i++) {
                total += speeds.get(i).getUploadSpeed();
            }
            realm.close();
            return total / speeds.size();
        }
        return 0f;
    }

    public float ping_average(Date date) {
        Realm realm = Realm.getDefaultInstance();
        Float total = 0F;
        ArrayList<Speed> speeds = new ArrayList<>();
        String strDate = DateFormat.getDateInstance().format(date);
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed != null) {
            speeds.addAll(dailySpeed.speeds);
            for (int i = 0; i < speeds.size(); i++) {
                total += speeds.get(i).getPing();
            }
            realm.close();
            return total / speeds.size();
        }
        return 0f;
    }

    public ArrayList<Float> downloads() {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Float> downloadArrayList = new ArrayList<>();
        RealmResults<DailySpeed> dailySpeeds = realm.where(DailySpeed.class).findAll();
        for (int i = 0; i < dailySpeeds.size(); i++) {
            for (int j = 0; j < dailySpeeds.get(i).speeds.size(); j++) {
                downloadArrayList.add(dailySpeeds.get(i).speeds.get(j).getDownloadSpeed());
            }
        }
        realm.close();
        return downloadArrayList;
    }

    public ArrayList<Float> uploads() {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Float> uploadArrayList = new ArrayList<>();
        RealmResults<DailySpeed> dailySpeeds = realm.where(DailySpeed.class).findAll();
        for (int i = 0; i < dailySpeeds.size(); i++) {
            for (int j = 0; j < dailySpeeds.get(i).speeds.size(); j++) {
                uploadArrayList.add(dailySpeeds.get(i).speeds.get(j).getUploadSpeed());
            }
        }
        realm.close();
        return uploadArrayList;
    }

    public ArrayList<Float> pings() {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Float> pingArrayList = new ArrayList<>();
        RealmResults<DailySpeed> dailySpeeds = realm.where(DailySpeed.class).findAll();
        for (int i = 0; i < dailySpeeds.size(); i++) {
            for (int j = 0; j < dailySpeeds.get(i).speeds.size(); j++) {
                pingArrayList.add(dailySpeeds.get(i).speeds.get(j).getPing());
            }
        }
        realm.close();
        return pingArrayList;
    }

    public ArrayList<Float> downloads(Date date) {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Float> downloadArrayList = new ArrayList<>();
        String strDate = DateFormat.getDateInstance().format(date);
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed != null) {
            for (int i = 0; i < dailySpeed.speeds.size(); i++) {
                downloadArrayList.add(dailySpeed.speeds.get(i).getDownloadSpeed());
            }
        }
        realm.close();
        return downloadArrayList;
    }

    public ArrayList<Float> uploads(Date date) {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Float> uploadArrayList = new ArrayList<>();
        String strDate = DateFormat.getDateInstance().format(date);
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed != null) {
            for (int i = 0; i < dailySpeed.speeds.size(); i++) {
                uploadArrayList.add(dailySpeed.speeds.get(i).getUploadSpeed());
            }
        }
        realm.close();
        return uploadArrayList;
    }

    public ArrayList<Float> pings(Date date) {
        Realm realm = Realm.getDefaultInstance();
        ArrayList<Float> pingArrayList = new ArrayList<>();
        String strDate = DateFormat.getDateInstance().format(date);
        DailySpeed dailySpeed = realm.where(DailySpeed.class).equalTo("date", strDate).findFirst();
        if (dailySpeed != null) {
            for (int i = 0; i < dailySpeed.speeds.size(); i++) {
                pingArrayList.add(dailySpeed.speeds.get(i).getPing());
            }
        }

        realm.close();
        return pingArrayList;
    }
}
