package com.minkhantlu.speedtestbenchmark.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.minkhantlu.speedtestbenchmark.R;
import com.minkhantlu.speedtestbenchmark.models.DataChart;

import java.util.ArrayList;

/**
 * Created by minkhantlu on 10/27/18.
 */

public class ChartAdapter extends RecyclerView.Adapter<ChartAdapter.TextViewHolder> {

    private Context context;
    private ArrayList<DataChart> dataCharts = new ArrayList<>();

    public ChartAdapter(Context context, ArrayList<DataChart> dataCharts) {
        this.context = context;
        this.dataCharts = dataCharts;
    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_chart, parent, false);
        return new TextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        holder.tvTitle.setText(dataCharts.get(position).title);

        ArrayList<Float> datas = dataCharts.get(position).floatArrayList;
        ArrayList<Entry> lineEntries = new ArrayList<Entry>();
        for (int i = 0; i < datas.size(); i++) {
            lineEntries.add(new Entry(i, datas.get(i)));
        }

        LineDataSet lineDataSet = new LineDataSet(lineEntries, holder.tvTitle.getText().toString());
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setHighlightEnabled(true);
        lineDataSet.setLineWidth(2);
        lineDataSet.setValueTextSize(8);
        lineDataSet.setDrawCircles(true);

        LineData lineData = new LineData(lineDataSet);

        holder.lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        holder.lineChart.animateY(1000);
        holder.lineChart.getXAxis().setGranularityEnabled(true);
        holder.lineChart.getXAxis().setGranularity(1.0f);
        holder.lineChart.getXAxis().setLabelCount(lineDataSet.getEntryCount());
        holder.lineChart.setData(lineData);
    }

    @Override
    public int getItemCount() {
        return dataCharts.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private LineChart lineChart;

        public TextViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_title);
            lineChart = itemView.findViewById(R.id.chart);
        }
    }

}
