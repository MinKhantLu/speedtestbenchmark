package com.minkhantlu.speedtestbenchmark.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.minkhantlu.speedtestbenchmark.R;
import com.minkhantlu.speedtestbenchmark.adapters.ChartAdapter;
import com.minkhantlu.speedtestbenchmark.models.DataChart;
import com.minkhantlu.speedtestbenchmark.util.Benchmark;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private TextView tvPing, tvDownload, tvUpload, tvDate;
    private Benchmark benchmark;
    private RecyclerView recyclerView;
    private ChartAdapter chartAdapter;
    private LineChart lineChart;
    private ArrayList<DataChart> chartDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        benchmark = new Benchmark();
        setContentView(R.layout.activity_main);

        lineChart = findViewById(R.id.chart);
        tvPing = findViewById(R.id.tv_ping);
        tvDownload = findViewById(R.id.tv_download);
        tvUpload = findViewById(R.id.tv_upload);
        tvDate = findViewById(R.id.tv_date);

        recyclerView = findViewById(R.id.rv_charts);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String strDate = DateFormat.getDateInstance().format(new Date());
        tvDate.setText(strDate);

        tvPing.setText(String.format("%.2f", benchmark.ping) + " ms");
        tvDownload.setText(String.format("%.2f", benchmark.downloadSpeed) + " kbs");
        tvUpload.setText(String.format("%.2f", benchmark.uploadSpeed) + " kbs");


        //data are being added manually
        DataChart pingChart = new DataChart();
        pingChart.title = "Ping speed";
        pingChart.floatArrayList = benchmark.pings(new Date());

        DataChart downloadChart = new DataChart();
        downloadChart.title = "Download speeds";
        downloadChart.floatArrayList = benchmark.downloads(new Date());

        DataChart uploadChart = new DataChart();
        uploadChart.title = "Upload speeds";
        uploadChart.floatArrayList = benchmark.uploads(new Date());
        //end

        chartDataArrayList.add(pingChart);
        chartDataArrayList.add(downloadChart);
        chartDataArrayList.add(uploadChart);

        chartAdapter = new ChartAdapter(this, chartDataArrayList);
        recyclerView.setAdapter(chartAdapter);


    }

}
